#!/bin/bash

set -euo pipefail
set -x

template=Dockerfile.template
image_name=docker.io/jdsieci/httpd

httpd_version=$1

latest_tag=$(skopeo inspect docker://httpd:latest | jq -r ".RepoTags[] | select(. | test(\"^${httpd_version}.[0-9]+$\"))" | sort -V | tail -1)

version=$(echo -n ${latest_tag} | cut -d '-' -f1)
ver_major=$(echo -n ${version} | cut -d '.' -f1)
ver_minor=$(echo -n ${version} | cut -d '.' -f2)
ver_patch=$(echo -n ${version} | cut -d '.' -f3)


sed 's|%%HTTPD_VERSION%%|'${latest_tag}'|' ${template} > Dockerfile

podman run --rm httpd:${latest_tag} cat /usr/local/apache2/conf/httpd.conf > httpd.conf


#sed -i 's|DirectoryIndex.*|DirectoryIndex index.php index.html|' httpd.conf

cat logging.conf >> httpd.conf
cat document_root.conf >> httpd.conf
cat >> httpd.conf <<EOF
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so
LoadModule rewrite_module modules/mod_rewrite.so

<IfModule dir_module>
    DirectoryIndex disabled
    DirectoryIndex index.html
    DirectoryIndex index.php
</IfModule>

IncludeOptional conf.d/*.conf
EOF

podman build -t ${image_name}:${version} \
  -t ${image_name}:${ver_major}.${ver_minor} \
  .

